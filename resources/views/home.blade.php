@extends('layouts.master')

@section('content')
<div class="container">
    <div class="jumbotron">
        <h1>Welcome to CreateQuestionnairesOnline!</h1>
        </br>
        <p>CreateQuestionnairesOnline is an application that allows you to create and gather feedback from questionnaires</p>
        <hr>
        <p>Click the button below to start making questionnaires now!</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="/questionnaire" role="button">Get started</a>
        </p>
    </div>

<div class="jumbotron">
    <h1>Take part in questionnaires</h1>
    <p>Take part in questionnaires made by users of the site</p>
        <a class="btn btn-primary btn-lg" href="/answer" role="button">Take part</a>
    </p>
</div>
</div>

@endsection
