@extends('layouts.master')
@section('title', 'questionnaire')
@section('content')
    </br>
    <div class="col-md-6">
        <h1>My Questionnaires</h1>
    </div>
    <div class="col-md-6">
        </br>
        <a href="questionnaire/new" class="btn btn-lg btn-success pull-right">Create a new Questionnaire</a>

    </div>
    <section>
        @if (isset ($questionnaire))



                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <td>Questionnaire title</td>
                        <td>Description</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($questionnaire as $questionnaire)
                        <tr>
                            <td>{{ $questionnaire->title }}</td>
                            <td>{{ $questionnaire->description }}</td>


                            <td align="center"> <a href=questionnaire/{{ $questionnaire->id }}/createquestions class="btn btn-primary">Add questions</a></td>

                            <td align="center"> <a href="questionnaire/{{ $questionnaire->id }}/edit" class="btn btn-warning">Edit</a></td>
                            <td align="center">

                                {!! Form::open(['method' => 'DELETE', 'route' => ['questionnaire.destroy', $questionnaire->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                            </td>

                            <td align="center"> <a href="questionnaire/{{ $questionnaire->id }}/responses" class="btn btn-secondary">View Responses</a></td>


                        </tr>
                        </tr>



                    @endforeach

                    </tbody>
                </table>
            @else
                <p> No questionnaire created yet </p>
            @endif
    </section>

@endsection