@extends('layouts.master')
@section('title', 'Take Part')
@section('content')
</br>
<div class="col-md-6">
    <h1>Available Questionnaires</h1>
</div>
<div class="col-md-6">
    </br>

</div>
<section>
    @if (isset ($questionnaire))

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td>Questionnaire title</td>
                <td>Description</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            @foreach ($questionnaire as $questionnaire)
                <tr>
                    <td>{{ $questionnaire->title }}</td>
                    <td>{{ $questionnaire->description }}</td>


                    <td align="center"> <a href="questionnaire/{{ $questionnaire->id }}/takepart" class="btn btn-warning">Take Part</a></td>


            @endforeach

            </tbody>
        </table>
    @else
        <p> No questionnaire available yet </p>
    @endif
</section>

@endsection