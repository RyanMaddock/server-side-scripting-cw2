@extends('layouts.master')
@section('title', 'createquestionnaire')
@section('content')

<!doctype html>
<html>
<head>

    <meta charset="UTF-8">
    <title>Create New Questionnaire</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<div class="container">
    <header class="row">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <ul class="nav navbar-nav">
                    <a class="navbar-brand" href="/questionnaire">questionnaire</a>
                    <li class="active"><a href="new">New Questionnaire</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <article class="row">
        <h1>Create a new Questionnaire</h1>

        @if ($errors->any())
            <div>
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['url' => 'questionnaire']) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>


        </br>
        </br>

        <div class="form-group">
            {!! Form::submit('Create Questionnaire', ['class' => 'btn btn-primary form-control']) !!}
        </div>




    </article>
</div><!-- close container -->

</body>
</html>