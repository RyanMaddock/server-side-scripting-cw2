@extends('layouts.master')
@section('title', 'createquestionnaire')
@section('content')

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin - edit {{ $questionnaire->title }}</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<div class="container">
    <header class="row">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <ul class="nav navbar-nav">
                    <a class="navbar-brand" href="#">Questionnaires</a>
                    <li class="active"><a href="/">Edit Questionnaire</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <article class="row">
        <h1>Edit - {{ $questionnaire->title }}</h1>

        <!-- errors -->
        @if ($errors->any())
            <div>
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <!-- form goes here -->
        {!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => 'questionnaire/' . $questionnaire->id]) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Edit Questionnaire', ['class' => 'btn btn-primary form-control']) !!}
        </div>


        {!! Form::close() !!}


    </article>

</body>
</html>