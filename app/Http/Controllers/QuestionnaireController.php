<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\questionnaire;
use App\questions;

class QuestionnaireController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // get all the questionnaire
        $questionnaire = questionnaire::all();

        return view('questionnaire', ['questionnaire' => $questionnaire]);
    }


    public function show()
    {
        return view('admin.questionnaire.new');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'bail|required|unique:questionnaire|min:3|max:255',
            'description' => 'required',
        ]);

        $input = $request->all();

        questionnaire::create($input);

        return redirect('questionnaire');
    }

    public function edit($id)
    {
        $questionnaire = questionnaire::findOrFail($id);

        return view('admin.questionnaire.edit', compact('questionnaire'));
    }

    public function update(Request $request, $id)
    {
        $questionnaire = questionnaire::findOrFail($id);

        $questionnaire->update($request->all());

        return redirect('questionnaire');
    }

    public function destroy($id)
    {
        $questionnaire = questionnaire::find($id);

        $questionnaire->delete();

        return redirect('questionnaire');
    }
}