<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\questions;

use App\questionnaire;


class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view ('admin.questionnaire.createquestions', compact('questionnaire'));
    }

    public function store(Request $request)
    {

        $input = $request->all();

        questions::create($input);

        return redirect('questionnaire');
    }


}
