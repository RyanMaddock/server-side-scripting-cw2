<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\questionnaire;

class AnswersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        // get all the questionnaire
        $questionnaire = questionnaire::all();

        return view('answer', ['questionnaire' => $questionnaire]);
    }


}