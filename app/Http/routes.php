<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'HomeController');




//create group to run validation middleware

Route::group(['middle' => ['web']], function () {

    Route::auth();

    Route::resource('questionnaire', 'QuestionnaireController');

    Route::resource('answer', 'AnswersController');

    Route::resource('questionnaire/{questionnaireid}/createquestions', 'QuestionsController');



});







