<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
    public $table = "questionnaire";

    protected $fillable = [
        'title',
        'description',
    ];
    public $timestamps = false;
}

