<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questions extends Model
{
    public $table = "questions";
    protected $fillable = [
        'question',
        'answer',
    ];


    public function users(){

        return $this->belongsTo('user', 'userid');

    }

    public function questionnaire(){

        return $this->belongsTo('questionnaire', 'questionnaireid');

    }


}
