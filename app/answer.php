<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $table = "answers";

    protected $fillable = [
        'answer',
    ];
    public $timestamps = false;
}
